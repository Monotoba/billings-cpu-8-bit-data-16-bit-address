# Billings CPU 8/16 

The Billings CPU 8/16 is a simple CPU created in Logisim as a teaching aid. 
The CPU sports an 8-bit data bus and a 16-bit address space. Making it quite
capable of running large programs and simple operating systems. This makes it
ideal for teaching.

This repository not only includes the CPU but also a complete computer system
created in Logisim. The computer includes a text terminal for user input and
output. If all circuits are recreated in Logisim-Evolution, an rgb graphics
display could be included.

_The CPU Block Diagram:_
![picture](images/main.png)

_Complete Computer with Terminal_
![picture](computer.png)